# [fossaegean](https:/fossaegean.gr) - Website Design

The latest webpage design for the [fossaegean](https:/fossaegean.gr) community.

![Preview](/img/preview.png)

Based on [Materialize](https://github.com/Dogfalo/materialize).

## License

Content made available under [Creative Commons Attribution-ShareAlike 4.0](http://creativecommons.org/licenses/by-sa/4.0).

Code released under the [MIT License](LICENSE).
